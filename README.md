##### GITSTARS ###########

Projeto realizado utilizando ReactJS, SASS, Axios, MDBootstrap e autenticação com Auth0.

## Para Rodar

yarn install && yarn start

### Requisitos

Foi usado a última versão do Node 11
Versões superiores a 10 do NodeJS devem funcionar.

Para executar os pre-processadores você deve ter o Sass instalado.

### Sobre

Realizei o projeto em poucas horas, por falta de conhecimento em GraphQL deixei para implementar o mesmo futuramente e optei por utilizar Axios. 
Não encontrei o endereço para Requisões de POST da APi V3 do GitHub, facilmente encontrado na V4 GraphQL. O projeto é mais um overview geral de meus conhecimentos. 

A implementação do mesmo com arquitetura flux ajudaria nos patterns de comunicação dos componentes, também ficando como uma resalva para futuras melhorias. 