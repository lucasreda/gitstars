import React, {Component} from "react";
import {MDBContainer, MDBRow, MDBCol, MDBInput} from "mdbreact";
import Logo from '../Components/Logo'
import iconLocation from '../styles/icons/loc.svg';
import iconEmail from '../styles/icons/email.svg';
import iconPerson from '../styles/icons/person.svg';
import iconSearch from '../styles/icons/searchIcon.svg';
import iconStar from '../styles/icons/star.svg';
import iconWeb from '../styles/icons/web.svg';
import iconNotFound from '../styles/icons/notfound.svg';
import axios from 'axios';

class Profile extends Component {

  constructor(props) {
    super(props)
    this.state = {
      profileData: {},
      repoData: [],
      userName: this.props.profileName,
      showError: false
    }
  }

  componentDidMount() {
    this.getProfile();
  }

  getProfile = () => {

    let userName = this.state.userName;
    let currentComponent = this;
    const baseUrl = 'https://api.github.com/users/';

    console.log(userName);
    axios.get(baseUrl + userName).then(function(response) {
      console.log(response.data);
      let resultado = response.data;
      currentComponent.setState({profileData: resultado, showError: false});
    }).catch(function(error) {
      // handle error
      currentComponent.setState({showError: true});
    });
    axios.get(baseUrl + userName + '/repos').then(function(repos) {
      console.log(repos.data);
      let resultado = repos.data;
      currentComponent.setState({repoData: resultado});
    });

  }

  updateSearch = (e) => {
    e.preventDefault();
    this.setState({userName: this.state.texto})
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.userName !== this.state.userName) {
      this.getProfile();
    }
  }

  render() {
    return (<div>

      <MDBContainer>
        <MDBRow>
          <div className="col-md-3">
            <Logo/>
          </div>
          <div className="col-md-9 col-sm-12">
            <form onSubmit={(e) => this.updateSearch(e)}>
              <MDBRow>
                <MDBCol size="11">

                  <MDBInput hint="Search" type="text" containerClass="mt-0" onChange={(e) => this.setState({texto: e.target.value})}/>

                </MDBCol>
                <MDBCol size="1">
                  <button className="searchBtnProf" type="submit"><img src={iconSearch}/></button>
                </MDBCol>
              </MDBRow>
            </form>
          </div>
        </MDBRow>
      </MDBContainer>

      {
        !this.state.showError && <MDBContainer>

            <MDBRow>
              <div className="col-md-3 purpleBox">

                <div className="purpleBox__first">
                  <img className="purpleBox__first--avatar" src={this.state.profileData.avatar_url}/>
                  <h2 className="purpleBox__first--userName">{this.state.profileData.name}</h2>
                  <h3 className="purpleBox__first--userLogin">{this.state.profileData.login}</h3>
                </div>
                <div className="purpleBox__second">
                  <p className="purpleBox__second--bio">{this.state.profileData.bio}</p>
                  {this.state.profileData.company &&
                  <p className="purpleBox__second--infos"><img src={iconPerson}/> {this.state.profileData.company}</p> }
                  {this.state.profileData.email &&
                  <p className="purpleBox__second--infos"><img src={iconLocation}/> {this.state.profileData.location}</p> }
                  {this.state.profileData.email &&
                  <p className="purpleBox__second--infos"><img src={iconEmail}/>{this.state.profileData.email}</p> }
                  {this.state.profileData.html_url &&
                  <p className="purpleBox__second--infos"><img src={iconWeb}/>{this.state.profileData.html_url}</p> }
                </div>

              </div>
              <div className="col-md-9 repoBox">

                <ul>
                  {
                    this.state.repoData.map(repo => {
                      return (<li className="repoBox__item" key={repo.id} title={repo.title}>
                        <h2 className="repoBox__item--name">
                          <a href={repo.html_url}>{repo.full_name}</a>
                        </h2>
                        <p className="repoBox__item--desc">
                          {repo.description}
                        </p>
                        <p className="repoBox__item-stars">
                          <img src={iconStar}/> {repo.stargazers_count}
                        </p>
                      </li>)
                    })
                  }
                </ul>

              </div>

            </MDBRow>
          </MDBContainer>

      }

      {
        this.state.showError && <MDBContainer>
            <div className="home text-center">

              <div className="notFound">
                <img src={iconNotFound}/>
              </div>

              <h2>User Not Found</h2>

            </div>

          </MDBContainer>

      }
    </div>);
  }
}

export default Profile;
