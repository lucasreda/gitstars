import React, {Component} from "react";
import Logo from '../Components/Logo'
import iconSearch from '../styles/icons/searchIcon.svg';
import {MDBContainer, MDBRow, MDBCol} from "mdbreact";

class Home extends Component {

  render() {
    return (<div className="home">
      <MDBContainer>
        <div className="row d-flex justify-content-center">
          <Logo/>
        </div>

        <div className="searchBox">
          <form onSubmit={(e) => this.props.showScreen(e, false, this.state.texto)}>
            <MDBRow>
              <div className="col-xs-12 col-md-11">
                <input className="searchBox__form" type="text" placeholder="Search a Git Username" onChange={(e) => this.setState({texto: e.target.value})}/>
              </div>
              <div className="col-sm-1 d-none d-md-block">
                <button className="searchBox__btn" type="submit"><img src={iconSearch}/></button>
              </div>
            </MDBRow>
          </form>
        </div>
      </MDBContainer>
    </div>);
  }
}

export default Home;
