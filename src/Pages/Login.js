import React, {Component} from "react";
import Logo from '../Components/Logo'

import Auth from '../Auth/Auth.js';

const auth = new Auth();
auth.login();

class Login extends Component {

  login() {
    this.props.auth.login();
  }

  render() {
    return (<Logo/>);
  }
}

export default Login;
