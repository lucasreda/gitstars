import React, {Component} from 'react';
import './styles/css/App.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import Home from './Pages/Home'
import Profile from './Pages/Profile'
import Logo from './Components/Logo'
import {MDBContainer} from "mdbreact";

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show: true,
      userName: []
    }

    this.showScreen = this.showScreen.bind(this);

  }

  showScreen = (e, value, userName) => {
    e.preventDefault();

    if (userName == null) {
      alert('entrei');
    }
    this.setState({show: value, userName: userName});

  }

  login() {
    this.props.auth.login();
  }

  render() {
    const {isAuthenticated} = this.props.auth;

    return (<div>
      {isAuthenticated() && this.state.show && <Home showScreen={this.showScreen}/>}
      {isAuthenticated() && !this.state.show && <Profile profileName={this.state.userName}/>}

      {
        !isAuthenticated() && (<MDBContainer >
          <div className="home">
            <Logo/>
            <div className="d-flex justify-content-center">
              <a className="loginButton" onClick={this.login.bind(this)}>
                Faça Login para continuar
              </a>
            </div>
          </div>
        </MDBContainer>)
      }
    </div>);
  }
}

export default App;
