import React from "react";

const Logo = () => {
  return (<div className="logo">
    <h1>GitHub<span className="purpleName">Stars</span>
    </h1>
  </div>);
}

export default Logo;
